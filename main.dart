import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:page_transition/page_transition.dart';

void main() {
    runApp (const MyApp());
}

class MyApp extends StatelessWidget {
    const MyApp({Key? key}) : super(key: key);

    @override
    widget build(BuildContext context) {
        return MaterialApp (
            title: 'Spaza Connexion',
            theme: ThemeData (
                primarySwatch: Colors.blue,  //color of the appBar
                scaffoldBackgroundColor: Colors.tealAccent, //theme of the scaffold
            ),
            home: const SplashScreen(),    
            )
        )
    }
}

class SplashScreen extends StatelessWidget {
    const SplashScreen({key? key}) : super (key: key);

    @override
    widget build(BuildContext context) {
        return AnimatedSplashScreen (
            splash: Column (
                childred: [
                    Image.asset('assets/spazaIcon.jpg'),
                    const Text ('spaza connexion app', style: TextStyle(fontSize: 40, fontWeight: fontWeight.bold),),
                    backgroundColor: Colors.red,
                nextScreen: const Home(),
                ],
            ),
        )

    }
}

class Home extends StatelessWidget {
    const Home({key? key}) : super (key: key);

    @override
    widget build(BuildContext context) {
        return Scaffold (
                appBar: AppBar (
                    leading: const Icon(Icons.menu),
                    title: const Text ('home')),
                
            body: const Center (
                child: Text ('welcome to Spaza Connexion')),
                floatingActionButton: FloatingActionButton(
                backgroundColor: const Color(0xff03dac6),
                
                    onPressed: () {
                    print ('button pressed');
                    },
                child: const Icon(Icons.navigation),
            )
        ), 
        
    }
}
